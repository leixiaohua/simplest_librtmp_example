### 最简单的基于LibRTMP的例子

#### 作者信息

+ 雷霄骅 leixiaohua1020@126.com 
+ 张晖 zhanghuicuc@gmail.com
  http://blog.csdn.net/leixiaohua1020

#### 环境

+ Win10
+ Visual Studio Community 2015

#### 项目介绍

本工程包含了LibRTMP的使用示例，包含如下子工程：

+ simplest_librtmp_receive: 接收RTMP流媒体并在本地保存成FLV格式的文件。

  [《RTMP保存为FLV》](https://blog.csdn.net/leixiaohua1020/article/details/42104893)

+ simplest_librtmp_send_flv: 将FLV格式的视音频文件使用RTMP推送至RTMP流媒体服务器。

  [《FLV通过RTMP发布》](https://blog.csdn.net/leixiaohua1020/article/details/42104945)

+ simplest_librtmp_send264: 将内存中的H.264数据推送至RTMP流媒体服务器。

  [《H264通过RTMP发布》](https://blog.csdn.net/leixiaohua1020/article/details/42105049)